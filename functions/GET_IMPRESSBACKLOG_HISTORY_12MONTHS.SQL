/* List of  NETSUITE schemas:

-- USE SCHEMA NETSUITE_SANDBOX;

-- USE SCHEMA NETSUITE_QA;

-- USE SCHEMA NETSUITE;
*/

/*
 * In case of deployment on NETSUITE_SCHEMA leave it as is
 * 
 * OTHERWISE
 * 
 * Replace commented schema NETSUITE_SCHEMA by the one from the list above 
 *  
*/

CREATE OR REPLACE FUNCTION GET_IMPRESSBACKLOG_HISTORY_12MONTHS (
							iCountry varchar,  -- values CAN or USA or ALL 
							iNetwork	varchar -- values ELEVATOR or LOBBY or ALL
							)
RETURNS TABLE (		asofdate 			date, 
					Total_Impression_BackLog integer
				)
  AS
$$
WITH 
	_CALENDAR  AS (
			SELECT  ASOFDATE,
					MONTHPRIORDATE,
					RN,
					WORKHOURS_IN_MONTH
			FROM VW_CALENDAR_PREV12MONTHES
	),
	_COUNTRIES AS ( 
						SELECT  230 AS  COUNTRY_ID, 'USA' AS COUNTRY
		    				 UNION ALL 
			  			SELECT  37 AS  COUNTRY_ID, 'CAN' AS COUNTRY   
	 ), 
	_NETWORK AS (	
					SELECT 'ELEVATOR' NETWORK, '%Cab%' name_mask 
						UNION ALL
					SELECT 'LOBBY' NETWORK, '%Lobby%' name_mask	
						UNION ALL
					SELECT 'ALL' NETWORK, '%' name_mask	
	), 
	_PROJECTS AS (	
					SELECT 		 
						T.END_DATE, SITE_ID, 	T.PROJECT_ID,	T.NAME, T.STATUS, T.CUSTOMER_ID, T.COUNTRY_ID, T.SQUARE_FEET
					FROM VW_BACKLOG			T 
						LEFT	JOIN	_COUNTRIES CTR	ON T.COUNTRY_ID	= CTR.COUNTRY_ID
					WHERE
						--
						( ctr.COUNTRY = iCountry OR iCountry = 'ALL')
						---
						AND EXISTS (
										SELECT 1 FROM /*NETSUITE_SCHEMA.*/PROJECT_TASKS x	
											INNER JOIN _NETWORK n 
											WHERE X._FIVETRAN_DELETED = FALSE
												AND x.PROJECT_ID = t.PROJECT_ID
												---
												AND n.NETWORK = iNetwork
												AND x.NAME	LIKE n.name_mask
												---
						)
	),
_IMPRESSES AS ( 
					SELECT   	X.CUSTOMER_ID, X.SITE_ID, X.PROJECT_ID, D.ASOFDATE,
								COUNT(1)	TOTAL_Lobby_COUNT,
								SUM(IFF((X.STATUS <> 'Completed' or (X.STATUS = 'Completed' and X.END_DATE > D.ASOFDATE)),1,0 ))	AS	BACKLOGGED_LobbyS, 
								SUM(IFF((X.END_DATE <= D.ASOFDATE),1,0 ))	AS	COMPLETED_LobbyS
					FROM _PROJECTS X  
						INNER JOIN _CALENDAR D 
					WHERE   X.NAME LIKE 'Lobby%' 
					GROUP BY X.CUSTOMER_ID, X.SITE_ID, X.PROJECT_ID, D.ASOFDATE
			) 
		SELECT	D.ASOFDATE AS PK, 
				round(SUM( 	((A.SQUARE_FEET/1000.00) * B.AME) * (BACKLOGGED_LobbyS/
									TOTAL_Lobby_COUNT) * (220 * D.WORKHOURS_IN_MONTH)
				),1)::INTEGER AS 		Total_Impression_BackLog		   
		 FROM _CALENDAR D
		  	INNER JOIN _PROJECTS A 
		  	INNER JOIN /*NETSUITE_SCHEMA.*/COUNTRIES C 
		  				ON C.COUNTRY_ID = A.COUNTRY_ID
		  	INNER JOIN  PIVOTAL_SFA_DBO.CP_VENUE_NETWORK  B 
		  				ON B.NetworkCountry =  CASE  WHEN A.COUNTRY_ID = '230' THEN 'US'
			                                          WHEN A.COUNTRY_ID = '37'  THEN 'Canada' END 
			LEFT JOIN _IMPRESSES	S0 
						ON S0.CUSTOMER_ID = A.CUSTOMER_ID 
							AND S0.SITE_ID = A.SITE_ID 
							AND S0.PROJECT_ID = A.PROJECT_ID 
							AND S0.ASOFDATE= D.ASOFDATE
			WHERE  (A.NAME LIKE '%Generate Master Service Agreement%'  or A.NAME LIKE '%MSA Signed%')
		            AND	A.END_DATE < D.ASOFDATE
			        AND	A.STATUS = 'Completed' 
					AND	B.Name in ('US Lobby','CAN Lobby Health')
					AND	BACKLOGGED_LobbyS>0
					AND B._FIVETRAN_DELETED = FALSE
					AND C._FIVETRAN_DELETED = FALSE
		GROUP BY D.ASOFDATE					   
		ORDER BY D.ASOFDATE DESC
$$
;