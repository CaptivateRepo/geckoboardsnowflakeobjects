CREATE OR REPLACE FUNCTION GET_BLDGBACKLOG_HISTORY_12MONTHS (
							iCountry varchar,  -- values CAN or USA or ALL 
							iNetwork	varchar -- values ELEVATOR or LOBBY or ALL
							)
RETURNS TABLE (		asofdate date, 
					Total_Bldg_BackLog integer
				)
  AS
$$
WITH 	
		_COUNTRIES AS ( 
						SELECT  230 AS  COUNTRY_ID, 'USA' AS COUNTRY
		    				 UNION ALL 
			  			SELECT  37 AS  COUNTRY_ID, 'CAN' AS COUNTRY   
		 ), 
		_NETWORK AS (	
						SELECT 'ELEVATOR' NETWORK, '%Cab%' name_mask 
							UNION ALL
						SELECT 'LOBBY' NETWORK, '%Lobby%' name_mask	
							UNION ALL
						SELECT 'ALL' NETWORK, '%' name_mask	
		),
		_PROJECTS AS ( 
						SELECT	
									S.*
						FROM 	VW_BACKLOG S
									LEFT JOIN _COUNTRIES CTR	
										ON S.COUNTRY_ID	= CTR.COUNTRY_ID
						WHERE 1=1 	
								---
								AND ( ctr.COUNTRY = iCountry OR iCountry = 'ALL')
								---
								AND EXISTS (
										SELECT 1 FROM FIVETRAN.NETSUITE.PROJECT_TASKS x	
											INNER JOIN _NETWORK n 
											WHERE X._FIVETRAN_DELETED = FALSE
												AND x.PROJECT_ID = S.PROJECT_ID
												---
												AND n.NETWORK = iNetwork
												AND S.NAME	LIKE n.name_mask
												---
								)
		),
		_calendar  AS (
					SELECT  ASOFDATE,
							MONTHPRIORDATE,
							RN,
							WORKHOURS_IN_MONTH
					FROM VW_CALENDAR_PREV12MONTHES
					)
		SELECT 	CURRENT_DATE AS PK, 
				count(DISTINCT T.SITE_ID)  AS Total_Bldg_BackLog
			FROM _PROJECTS T 
			WHERE  	  	( T.NAME LIKE '%Generate Master Service Agreement%'  or T.NAME LIKE '%MSA Signed%' )
						AND T.END_DATE <  CURRENT_DATE
			          	AND   T.STATUS = 'Completed'
						AND EXISTS (
									SELECT  'X' FROM _PROJECTS B
							                  WHERE B.PROJECT_ID = T.PROJECT_ID
											    AND (B.STATUS <> 'Completed' 
											    		OR (B.STATUS = 'Completed' and B.END_DATE > CURRENT_DATE  
											    		) 
											    	) 
						)
		UNION ALL						
		SELECT 	ADD_MONTHS(CURRENT_DATE,-1) AS PK, 
				count(DISTINCT T.SITE_ID)  AS Total_Bldg_BackLog
			FROM _PROJECTS T 
			WHERE  	  	( T.NAME LIKE '%Generate Master Service Agreement%'  or T.NAME LIKE '%MSA Signed%' )
						AND T.END_DATE <  ADD_MONTHS(CURRENT_DATE,-1)
			          	AND   T.STATUS = 'Completed'
						AND EXISTS (
									SELECT  'X' FROM _PROJECTS B
							                  WHERE B.PROJECT_ID = T.PROJECT_ID
											    AND (B.STATUS <> 'Completed' 
											    		OR (B.STATUS = 'Completed' and B.END_DATE > ADD_MONTHS(CURRENT_DATE,-1)  
											    		) 
											    	) 
						)
		UNION ALL						
		SELECT 	ADD_MONTHS(CURRENT_DATE,-2) AS PK, 
				count(DISTINCT T.SITE_ID)  AS Total_Bldg_BackLog
			FROM _PROJECTS T 
			WHERE  	  	( T.NAME LIKE '%Generate Master Service Agreement%'  or T.NAME LIKE '%MSA Signed%' )
						AND T.END_DATE <  ADD_MONTHS(CURRENT_DATE,-2)
			          	AND   T.STATUS = 'Completed'
						AND EXISTS (
									SELECT  'X' FROM _PROJECTS B
							                  WHERE B.PROJECT_ID = T.PROJECT_ID
											    AND (B.STATUS <> 'Completed' 
											    		OR (B.STATUS = 'Completed' and B.END_DATE > ADD_MONTHS(CURRENT_DATE,-2)  
											    		) 
											    	) 
						)
		UNION ALL						
		SELECT 	ADD_MONTHS(CURRENT_DATE,-3) AS PK, 
				count(DISTINCT T.SITE_ID)  AS Total_Bldg_BackLog
			FROM _PROJECTS T 
			WHERE  	  	( T.NAME LIKE '%Generate Master Service Agreement%'  or T.NAME LIKE '%MSA Signed%' )
						AND T.END_DATE <  ADD_MONTHS(CURRENT_DATE,-3)
			          	AND   T.STATUS = 'Completed'
						AND EXISTS (
									SELECT  'X' FROM _PROJECTS B
							                  WHERE B.PROJECT_ID = T.PROJECT_ID
											    AND (B.STATUS <> 'Completed' 
											    		OR (B.STATUS = 'Completed' and B.END_DATE > ADD_MONTHS(CURRENT_DATE,-3)  
											    		) 
											    	) 
						)
		UNION ALL						
		SELECT 	ADD_MONTHS(CURRENT_DATE,-4) AS PK, 
				count(DISTINCT T.SITE_ID)  AS Total_Bldg_BackLog
			FROM _PROJECTS T 
			WHERE  	  	( T.NAME LIKE '%Generate Master Service Agreement%'  or T.NAME LIKE '%MSA Signed%' )
						AND T.END_DATE <  ADD_MONTHS(CURRENT_DATE,-4)
			          	AND   T.STATUS = 'Completed'
						AND EXISTS (
									SELECT  'X' FROM _PROJECTS B
							                  WHERE B.PROJECT_ID = T.PROJECT_ID
											    AND (B.STATUS <> 'Completed' 
											    		OR (B.STATUS = 'Completed' and B.END_DATE > ADD_MONTHS(CURRENT_DATE,-4)
											    		) 
											    	) 
						)
		UNION ALL						
		SELECT 	ADD_MONTHS(CURRENT_DATE,-5) AS PK, 
				count(DISTINCT T.SITE_ID)  AS Total_Bldg_BackLog
			FROM _PROJECTS T 
			WHERE  	  	( T.NAME LIKE '%Generate Master Service Agreement%'  or T.NAME LIKE '%MSA Signed%' )
						AND T.END_DATE <  ADD_MONTHS(CURRENT_DATE,-5)
			          	AND   T.STATUS = 'Completed'
						AND EXISTS (
									SELECT  'X' FROM _PROJECTS B
							                  WHERE B.PROJECT_ID = T.PROJECT_ID
											    AND (B.STATUS <> 'Completed' 
											    		OR (B.STATUS = 'Completed' and B.END_DATE > ADD_MONTHS(CURRENT_DATE,-5)
											    		) 
											    	) 
						)
		UNION ALL						
		SELECT 	ADD_MONTHS(CURRENT_DATE,-6) AS PK, 
				count(DISTINCT T.SITE_ID)  AS Total_Bldg_BackLog
			FROM _PROJECTS T 
			WHERE  	  	( T.NAME LIKE '%Generate Master Service Agreement%'  or T.NAME LIKE '%MSA Signed%' )
						AND T.END_DATE <  ADD_MONTHS(CURRENT_DATE,-6)
			          	AND   T.STATUS = 'Completed'
						AND EXISTS (
									SELECT  'X' FROM _PROJECTS B
							                  WHERE B.PROJECT_ID = T.PROJECT_ID
											    AND (B.STATUS <> 'Completed' 
											    		OR (B.STATUS = 'Completed' and B.END_DATE > ADD_MONTHS(CURRENT_DATE,-6)  
											    		) 
											    	) 
						)
		UNION ALL						
		SELECT 	ADD_MONTHS(CURRENT_DATE,-7) AS PK, 
				count(DISTINCT T.SITE_ID)  AS Total_Bldg_BackLog
			FROM _PROJECTS T 
			WHERE  	  	( T.NAME LIKE '%Generate Master Service Agreement%'  or T.NAME LIKE '%MSA Signed%' )
						AND T.END_DATE <  ADD_MONTHS(CURRENT_DATE,-7)
			          	AND   T.STATUS = 'Completed'
						AND EXISTS (
									SELECT  'X' FROM _PROJECTS B
							                  WHERE B.PROJECT_ID = T.PROJECT_ID
											    AND (B.STATUS <> 'Completed' 
											    		OR (B.STATUS = 'Completed' and B.END_DATE > ADD_MONTHS(CURRENT_DATE,-7)
											    		) 
											    	) 
						)
		UNION ALL						
		SELECT 	ADD_MONTHS(CURRENT_DATE,-8) AS PK, 
				count(DISTINCT T.SITE_ID)  AS Total_Bldg_BackLog
			FROM _PROJECTS T 
			WHERE  	  	( T.NAME LIKE '%Generate Master Service Agreement%'  or T.NAME LIKE '%MSA Signed%' )
						AND T.END_DATE <  ADD_MONTHS(CURRENT_DATE,-8)
			          	AND   T.STATUS = 'Completed'
						AND EXISTS (
									SELECT  'X' FROM _PROJECTS B
							                  WHERE B.PROJECT_ID = T.PROJECT_ID
											    AND (B.STATUS <> 'Completed' 
											    		OR (B.STATUS = 'Completed' and B.END_DATE > ADD_MONTHS(CURRENT_DATE,-8)
											    		) 
											    	) 
						)
		UNION ALL						
		SELECT 	ADD_MONTHS(CURRENT_DATE,-9) AS PK, 
				count(DISTINCT T.SITE_ID)  AS Total_Bldg_BackLog
			FROM _PROJECTS T 
			WHERE  	  	( T.NAME LIKE '%Generate Master Service Agreement%'  or T.NAME LIKE '%MSA Signed%' )
						AND T.END_DATE <  ADD_MONTHS(CURRENT_DATE,-9)
			          	AND   T.STATUS = 'Completed'
						AND EXISTS (
									SELECT  'X' FROM _PROJECTS B
							                  WHERE B.PROJECT_ID = T.PROJECT_ID
											    AND (B.STATUS <> 'Completed' 
											    		OR (B.STATUS = 'Completed' and B.END_DATE > ADD_MONTHS(CURRENT_DATE,-9)  
											    		) 
											    	) 
						)
		UNION ALL						
		SELECT 	ADD_MONTHS(CURRENT_DATE,-10) AS PK, 
				count(DISTINCT T.SITE_ID)  AS Total_Bldg_BackLog
			FROM _PROJECTS T 
			WHERE  	  	( T.NAME LIKE '%Generate Master Service Agreement%'  or T.NAME LIKE '%MSA Signed%' )
						AND T.END_DATE <  ADD_MONTHS(CURRENT_DATE,-10)
			          	AND   T.STATUS = 'Completed'
						AND EXISTS (
									SELECT  'X' FROM _PROJECTS B
							                  WHERE B.PROJECT_ID = T.PROJECT_ID
											    AND (B.STATUS <> 'Completed' 
											    		OR (B.STATUS = 'Completed' and B.END_DATE > ADD_MONTHS(CURRENT_DATE,-10)
											    		) 
											    	) 
						)
		UNION ALL						
		SELECT 	ADD_MONTHS(CURRENT_DATE,-11) AS PK, 
				count(DISTINCT T.SITE_ID)  AS Total_Bldg_BackLog
			FROM _PROJECTS T 
			WHERE  	  	( T.NAME LIKE '%Generate Master Service Agreement%'  or T.NAME LIKE '%MSA Signed%' )
						AND T.END_DATE <  ADD_MONTHS(CURRENT_DATE,-11)
			          	AND   T.STATUS = 'Completed'
						AND EXISTS (
									SELECT  'X' FROM _PROJECTS B
							                  WHERE B.PROJECT_ID = T.PROJECT_ID
											    AND (B.STATUS <> 'Completed' 
											    		OR (B.STATUS = 'Completed' and B.END_DATE > ADD_MONTHS(CURRENT_DATE,-11)
											    		) 
											    	) 
						)
		UNION ALL						
		SELECT 	ADD_MONTHS(CURRENT_DATE,-12) AS PK, 
				count(DISTINCT T.SITE_ID)  AS Total_Bldg_BackLog
			FROM _PROJECTS T 
			WHERE  	  	( T.NAME LIKE '%Generate Master Service Agreement%'  or T.NAME LIKE '%MSA Signed%' )
						AND T.END_DATE <  ADD_MONTHS(CURRENT_DATE,-12)
			          	AND   T.STATUS = 'Completed'
						AND EXISTS (
									SELECT  'X' FROM _PROJECTS B
							                  WHERE B.PROJECT_ID = T.PROJECT_ID
											    AND (B.STATUS <> 'Completed' 
											    		OR (B.STATUS = 'Completed' and B.END_DATE > ADD_MONTHS(CURRENT_DATE,-12)
											    		) 
											    	) 
						)
$$
;