CREATE OR REPLACE VIEW VW_GB_LOBBY_CAB_METRICS_CURMO_VS_YRAGO AS
WITH _PREPARED_4JSON AS ( 
		SELECT	
				b.PK,   
				b.TotalCabs,
				b.Cabs_Inactive_MSA,
				b.Cabs_Exp_Nxt12_Count,
				b.TotalCabsPendingRetirement,
				b.CabOutageHrs_Elev_Bldgs,
				b.CabsOutOfService,
				b.Cab_Reliability,				
				h.Total_Lobby_BackLog
			FROM 
				VW_LOBBY_CAB_METRICS_12MOS	 b
					LEFT JOIN VW_LOBBY_CAB_BACKLOG_HISTORY_12MONTHS h 
						ON b.PK = h.PK  
			WHERE b.rn IN (1,13)
)
SELECT 
		array_agg(
			OBJECT_CONSTRUCT(
			'pk', to_char(PK, 'yyyy-mm-dd'),
			'totalcabs',		(TotalCabs),
			'cabs_inactive_msa',			(Cabs_Inactive_MSA),
			'cabs_exp_nxt12_count',		(Cabs_Exp_Nxt12_Count),
			'totalcabspendingretirement',		(TotalCabsPendingRetirement),
			'total_cab_backlog',	(Total_Lobby_BackLog), /*Alias!!!!*/
			'cabsoutofservice',		(CabsOutOfService), 
			'caboutagehrs_elev_bldgs',		(CabOutageHrs_Elev_Bldgs), 
			'cab_reliability',			(Cab_Reliability)
		)) AS data 
FROM 
	_PREPARED_4JSON;
