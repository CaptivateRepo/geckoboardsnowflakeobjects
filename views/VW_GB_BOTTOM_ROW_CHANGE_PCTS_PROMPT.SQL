CREATE OR REPLACE VIEW VW_GB_BOTTOM_ROW_CHANGE_PCTS_PROMPT COMMENT = 'Needs variables TALEND_COUNTRY,	TALEND_NETWORK' AS 
WITH 
_BLDGBACKLOG_HISTORY_12MONTHS AS(
				SELECT 
						asofdate AS PK,
						Total_Bldg_BackLog
				FROM  TABLE(GET_BLDGBACKLOG_HISTORY_12MONTHS(
									$TALEND_COUNTRY,
									$TALEND_NETWORK
							))
),	
_LOBBY_BLDG_METRICS	AS (
			SELECT		b.PK,   					
					b.Total_Bldg_BackLog,		lead(b.Total_Bldg_BackLog,1) 	OVER (ORDER BY rn) AS Total_Bldg_BackLog_prv,	lead(b.Total_Bldg_BackLog,2) 	OVER (ORDER BY rn) AS Total_Bldg_BackLog_lst,
					s.Bldg_Count, 				lead(s.Bldg_Count,1) 			OVER (ORDER BY rn) AS Bldg_Count_prv,			lead(s.Bldg_Count,2) 			OVER (ORDER BY rn) AS Bldg_Count_lst,
					s.L7BuildingsPending, 		lead(s.L7BuildingsPending,1) 	OVER (ORDER BY rn) AS L7BuildingsPending_prv,	lead(s.L7BuildingsPending,2) 	OVER (ORDER BY rn) AS L7BuildingsPending_lst,
					s.Bldgs_Inactive_MSA, 		lead(s.Bldgs_Inactive_MSA,1) 	OVER (ORDER BY rn) AS Bldgs_Inactive_MSA_prv,	lead(s.Bldgs_Inactive_MSA,2) 	OVER (ORDER BY rn) AS Bldgs_Inactive_MSA_lst,
					s.Bldg_Exp_Nxt12_Count, 	lead(s.Bldg_Exp_Nxt12_Count,1)OVER (ORDER BY rn) AS Bldg_Exp_Nxt12_Count_prv,	lead(s.Bldg_Exp_Nxt12_Count,2)	OVER (ORDER BY rn) AS Bldg_Exp_Nxt12_Count_lst,
					s.ServerOutageHrs, 			lead(s.ServerOutageHrs,1) 	OVER (ORDER BY rn) AS ServerOutageHrs_prv,		lead(s.ServerOutageHrs,2) 	OVER (ORDER BY rn) AS ServerOutageHrs_lst,
					s.Bldg_Reliability,			lead(s.Bldg_Reliability,1) 	OVER (ORDER BY rn) AS Bldg_Reliability_prv,		lead(s.Bldg_Reliability,2) 	OVER (ORDER BY rn) AS Bldg_Reliability_lst,
					s.rn
			FROM	VW_BLDG_METRICS_12MOS	 s
					LEFT JOIN _BLDGBACKLOG_HISTORY_12MONTHS b 
									ON b.PK = s.PK	 
			WHERE s.rn IN (1,2,13) 
),
_CABBACKLOG_HISTORY_12MONTHS AS
			(
				SELECT 
						asofdate AS PK,
						Total_Lobby_BackLog
				FROM  TABLE(GET_CABBACKLOG_HISTORY_12MONTHS(
									$TALEND_COUNTRY,
									$TALEND_NETWORK
						))
),
_LOBBY_CAB_METRICS	AS	(
		SELECT	b.PK,   
				b.TotalCabs,					lead(b.TotalCabs,1) 				OVER (ORDER BY rn) AS TotalCabs_prv,			lead(b.TotalCabs,2) 				OVER (ORDER BY rn) AS TotalCabs_lst,
				b.Cabs_Inactive_MSA,			lead(b.Cabs_Inactive_MSA,1) 		OVER (ORDER BY rn) AS Cabs_Inactive_MSA_prv,	lead(b.Cabs_Inactive_MSA,2) 		OVER (ORDER BY rn) AS Cabs_Inactive_MSA_lst,
				b.Cabs_Exp_Nxt12_Count,			lead(b.Cabs_Exp_Nxt12_Count,1) 	OVER (ORDER BY rn) AS Cabs_Exp_Nxt12_Count_prv,	lead(b.Cabs_Exp_Nxt12_Count,2) 	OVER (ORDER BY rn) AS Cabs_Exp_Nxt12_Count_lst,
				b.TotalCabsPendingRetirement,	lead(b.TotalCabsPendingRetirement,1) 	OVER (ORDER BY rn) AS TotalCabsPendingRetirement_prv,	lead(b.TotalCabsPendingRetirement,2) 	OVER (ORDER BY rn) AS TotalCabsPendingRetirement_lst,
				b.CabOutageHrs_Elev_Bldgs,		lead(b.CabOutageHrs_Elev_Bldgs,1) OVER (ORDER BY rn) AS CabOutageHrs_Elev_Bldgs_prv,	lead(b.CabOutageHrs_Elev_Bldgs,2) 	OVER (ORDER BY rn) AS CabOutageHrs_Elev_Bldgs_lst,
				b.CabsOutOfService,				lead(b.CabsOutOfService,1) 		OVER (ORDER BY rn) AS CabsOutOfService_prv,		lead(b.CabsOutOfService,2) 		OVER (ORDER BY rn) AS CabsOutOfService_lst,
				b.Cab_Reliability,				lead(b.Cab_Reliability,1) 		OVER (ORDER BY rn) AS Cab_Reliability_prv,		lead(b.Cab_Reliability,2) 		OVER (ORDER BY rn) AS Cab_Reliability_lst,
				h.Total_Lobby_BackLog AS Total_Cab_BackLog,	lead(h.Total_Lobby_BackLog,1) 	OVER (ORDER BY rn) AS Total_Cab_BackLog_prv,	lead(h.Total_Lobby_BackLog,2) 	OVER (ORDER BY rn) AS Total_Cab_BackLog_lst,
				b.rn
		FROM 
			VW_CAB_METRICS_12MOS	 b
					LEFT JOIN _CABBACKLOG_HISTORY_12MONTHS h 
									ON b.PK = h.PK	
				WHERE b.rn IN (1,2,13)
),
_IMPRESSBACKLOG_HISTORY_12MONTHS AS
			(
				SELECT 
						asofdate AS PK,
						Total_Impression_BackLog
				FROM  TABLE(GET_IMPRESSBACKLOG_HISTORY_12MONTHS(
									$TALEND_COUNTRY,
									$TALEND_NETWORK
						))
			),
_LOBBY_IMPRESSION_METRICS AS (
		SELECT	b.PK,   
				b.Impression_Count,				lead(b.Impression_Count,1) 			OVER (ORDER BY rn) AS Impression_Count_prv,			lead(b.Impression_Count,2) 			OVER (ORDER BY rn) AS Impression_Count_lst,
				b.L7ImpressionsPending, 		lead(b.L7ImpressionsPending,1) 		OVER (ORDER BY rn) AS L7ImpressionsPending_prv,		lead(b.L7ImpressionsPending,2) 		OVER (ORDER BY rn) AS L7ImpressionsPending_lst,
				b.Impressions_Inactive_MSA,		lead(b.Impressions_Inactive_MSA,1) 	OVER (ORDER BY rn) AS Impressions_Inactive_MSA_prv,	lead(b.Impressions_Inactive_MSA,2) 	OVER (ORDER BY rn) AS Impressions_Inactive_MSA_lst,
				b.Impressions_Exp_Nxt12_Count,	lead(b.Impressions_Exp_Nxt12_Count,1) OVER (ORDER BY rn) AS Impressions_Exp_Nxt12_Count_prv,lead(b.Impressions_Exp_Nxt12_Count,2) OVER (ORDER BY rn) AS Impressions_Exp_Nxt12_Count_lst,
				b.Impressions_MakeGoods,		lead(b.Impressions_MakeGoods,1) 		OVER (ORDER BY rn) AS Impressions_MakeGoods_prv,	lead(b.Impressions_MakeGoods,2) 		OVER (ORDER BY rn) AS Impressions_MakeGoods_lst,
				b.Impression_Utilization AS Inventory_Utilization,	lead(b.Impression_Utilization,1)	OVER (ORDER BY rn) AS Inventory_Utilization_prv,lead(b.Impression_Utilization,2)	OVER (ORDER BY rn) AS Inventory_Utilization_lst,
				b.actual_utilization,			lead(b.actual_utilization,1) 			OVER (ORDER BY rn) AS actual_utilization_prv,		lead(b.actual_utilization,2) 			OVER (ORDER BY rn) AS actual_utilization_lst,
				h.Total_Impression_BackLog,		lead(h.Total_Impression_BackLog,1) 	OVER (ORDER BY rn) AS Total_Impression_BackLog_prv,	lead(h.Total_Impression_BackLog,2) 	OVER (ORDER BY rn) AS Total_Impression_BackLog_lst,
				b.rn
		FROM VW_IMPRESSION_METRICS_12MOS b 
				LEFT JOIN  _IMPRESSBACKLOG_HISTORY_12MONTHS h
					ON b.PK = h.PK 
				WHERE b.rn IN (1,2,13)
),
_LOBBY_BOTTOM_ROW_CHANGE_PCTS AS ( 	
			SELECT      		
								'Buildings' as Metric_Type,
								/* Prev month*/
								(((b.Bldg_Count - b.Bldg_Count_prv) / NULLIF(b.Bldg_Count_prv,0)) * 100) as pct_change_Count_prv,
								(((b.L7BuildingsPending - b.L7BuildingsPending_prv) / NULLIF(b.L7BuildingsPending_prv,0)) * 100) as pct_change_Pending_retirement_prv,
								(((b.Total_Bldg_BackLog - b.Total_Bldg_BackLog_prv) / NULLIF(b.Total_Bldg_BackLog_prv,0)) * 100) as pct_change_backlog_prv,
								(((b.Bldgs_Inactive_MSA - b.Bldgs_Inactive_MSA_prv) / NULLIF(b.Bldgs_Inactive_MSA_prv,0)) * 100) as pct_change_Inactive_MSA_prv,
								(((b.Bldg_Exp_Nxt12_Count - b.Bldg_Exp_Nxt12_Count_prv) / NULLIF(b.Bldg_Exp_Nxt12_Count_prv,0)) * 100) as pct_change_Exp_Nxt12_Count_prv,
								(((b.ServerOutageHrs - b.ServerOutageHrs_prv) / NULLIF(b.ServerOutageHrs_prv,0)) * 100) as pct_change_OutageHrs_prv,
								(((b.Bldg_Reliability - b.Bldg_Reliability_prv) / NULLIF(b.Bldg_Reliability_prv,0)) * 100) as pct_change_Reliability_prv,
								/* Prev year*/	
								(((b.Bldg_Count - b.Bldg_Count_lst) / NULLIF(b.Bldg_Count_lst,0)) * 100) as pct_change_Count,
								(((b.L7BuildingsPending - b.L7BuildingsPending_lst) / NULLIF(b.L7BuildingsPending_lst,0)) * 100) as pct_change_Pending_retirement,
								(((b.Total_Bldg_BackLog - b.Total_Bldg_BackLog_lst) / NULLIF(b.Total_Bldg_BackLog_lst,0)) * 100) as pct_change_backlog,
								(((b.Bldgs_Inactive_MSA - b.Bldgs_Inactive_MSA_lst) /NULLIF( b.Bldgs_Inactive_MSA_lst,0)) * 100) as pct_change_Inactive_MSA,
								(((b.Bldg_Exp_Nxt12_Count - b.Bldg_Exp_Nxt12_Count_lst) / NULLIF(b.Bldg_Exp_Nxt12_Count_lst,0)) * 100) as pct_change_Exp_Nxt12_Count,
								(((b.ServerOutageHrs - b.ServerOutageHrs_lst) / NULLIF(b.ServerOutageHrs_lst,0)) * 100) as pct_change_OutageHrs,
								(((b.Bldg_Reliability - b.Bldg_Reliability_lst) / NULLIF(b.Bldg_Reliability_lst,0)) * 100) as pct_change_Reliability,
								PK
			FROM   _LOBBY_BLDG_METRICS b  
				WHERE b.rn = 1
			UNION ALL
			SELECT	'Cabs' as Metric_Type,
					/* Prev month*/
				    (((b.TotalCabs - b.TotalCabs_prv) / NULLIF(b.TotalCabs_prv,0)) * 100) as pct_change_count_prv,
				    (((b.TotalCabsPendingRetirement - b.TotalCabsPendingRetirement_prv) / NULLIF(b.TotalCabsPendingRetirement_prv,0)) * 100) as pct_change_Pending_retirement_prv,
				    (((b.Total_Cab_BackLog - b.Total_Cab_BackLog_prv) / NULLIF(b.Total_Cab_BackLog_prv,0)) * 100) as pct_change_backlog_prv,
				    (((b.Cabs_Inactive_MSA - b.Cabs_Inactive_MSA_prv) / NULLIF(b.Cabs_Inactive_MSA_prv,0)) * 100) as pct_change_Inactive_MSA_prv,
				    (((b.Cabs_Exp_Nxt12_Count - b.Cabs_Exp_Nxt12_Count_prv) / NULLIF(b.Cabs_Exp_Nxt12_Count_prv,0)) * 100) as pct_change_Exp_Nxt12_Count_prv,
				    (((b.CabOutageHrs_Elev_Bldgs - b.CabOutageHrs_Elev_Bldgs_prv) / NULLIF(b.CabOutageHrs_Elev_Bldgs_prv,0)) * 100) as pct_change_OutageHrs_prv,
				    (((b.Cab_Reliability - b.Cab_Reliability_prv) / NULLIF(b.Cab_Reliability_prv,0)) * 100) as pct_change_Reliability_prv,
					/* Prev year*/
				    (((b.TotalCabs - b.TotalCabs_lst) / NULLIF( b.TotalCabs_lst,0)) * 100) as pct_change_count,
				    (((b.TotalCabsPendingRetirement - b.TotalCabsPendingRetirement_lst) / NULLIF( b.TotalCabsPendingRetirement_lst,0)) * 100) as pct_change_Pending_retirement,
				    (((b.Total_Cab_BackLog - b.Total_Cab_BackLog_lst) / NULLIF( b.Total_Cab_BackLog_lst,0)) * 100) as pct_change_backlog,
				    (((b.Cabs_Inactive_MSA - b.Cabs_Inactive_MSA_lst) / NULLIF( b.Cabs_Inactive_MSA_lst,0)) * 100) as pct_change_Inactive_MSA,
				    (((b.Cabs_Exp_Nxt12_Count - b.Cabs_Exp_Nxt12_Count_lst) / NULLIF( b.Cabs_Exp_Nxt12_Count_lst,0)) * 100) as pct_change_Exp_Nxt12_Count,
				    (((b.CabOutageHrs_Elev_Bldgs - b.CabOutageHrs_Elev_Bldgs_lst) / NULLIF( b.CabOutageHrs_Elev_Bldgs_lst,0)) * 100) as pct_change_OutageHrs,
				    (((b.Cab_Reliability - b.Cab_Reliability_lst) / NULLIF( b.Cab_Reliability_lst,0)) * 100) as pct_change_Reliability,
					PK
			FROM	_LOBBY_CAB_METRICS b 
				WHERE b.rn = 1
			UNION ALL
			SELECT 'Impressions' as Metric_Type,
				/* Prev month*/
			   	(((b.Impression_count - b.Impression_count_prv) / NULLIF( b.Impression_count_prv,0)) * 100) as pct_change_count_prv,
			    (((b.L7ImpressionsPending - b.L7ImpressionsPending_prv) / NULLIF( b.L7ImpressionsPending_prv,0)) * 100) as pct_change_Pending_retirement_prv,
			    (((b.Total_Impression_BackLog - b.Total_Impression_BackLog_prv) / NULLIF( b.Total_Impression_BackLog_prv,0)) * 100) as pct_change_backlog_prv,
				(((b.Impressions_Inactive_MSA - b.Impressions_Inactive_MSA_prv) / NULLIF( b.Impressions_Inactive_MSA_prv,0)) * 100) as pct_change_Inactive_MSA_prv,
				(((b.Impressions_Exp_Nxt12_Count - b.Impressions_Exp_Nxt12_Count_prv) / NULLIF( b.Impressions_Exp_Nxt12_Count_prv,0)) * 100) as pct_change_Exp_Nxt12_Count_prv, 
			    (((b.Impressions_MakeGoods - b.Impressions_MakeGoods_prv) / NULLIF( b.Impressions_MakeGoods_prv,0)) * 100) as pct_change_OutageHrs_prv,
			    (((b.Inventory_Utilization - b.Inventory_Utilization_prv) / NULLIF( b.Inventory_Utilization_prv,0)) * 100) as pct_change_Reliability_prv,
				/* Prev year*/
			    (((b.Impression_count - b.Impression_count_lst) / NULLIF( b.Impression_count_lst,0)) * 100) as pct_change_count,
			    (((b.L7ImpressionsPending - b.L7ImpressionsPending_lst) / NULLIF( b.L7ImpressionsPending_lst,0)) * 100) as pct_change_Pending_retirement,
			    (((b.Total_Impression_BackLog - b.Total_Impression_BackLog_lst) / NULLIF( b.Total_Impression_BackLog_lst,0)) * 100) as pct_change_backlog,
			    (((b.Impressions_Inactive_MSA - b.Impressions_Inactive_MSA_lst) / NULLIF( b.Impressions_Inactive_MSA_lst,0)) * 100) as pct_change_Inactive_MSA,
			    (((b.Impressions_Exp_Nxt12_Count - b.Impressions_Exp_Nxt12_Count_lst) / NULLIF( b.Impressions_Exp_Nxt12_Count_lst,0)) * 100) as pct_change_Exp_Nxt12_Count,
			    (((b.Impressions_MakeGoods - b.Impressions_MakeGoods_lst) / NULLIF( b.Impressions_MakeGoods_lst,0)) * 100) as pct_change_OutageHrs,
			    (((b.Inventory_Utilization - b.Inventory_Utilization_lst) / NULLIF( b.Inventory_Utilization_lst,0)) * 100) as pct_change_Reliability,
				PK
			FROM _LOBBY_IMPRESSION_METRICS b 
			WHERE b.rn = 1    )
SELECT	array_agg(
				OBJECT_CONSTRUCT(
				'metric_type',					(Metric_Type),
				'pct_change_count',				(pct_change_count),
				'pct_change_inactive_sfa',		(pct_change_Pending_retirement),
				'pct_change_backlog',			(pct_change_backlog),
				'pct_change_inactive_msa',		(pct_change_Inactive_MSA),
				'pct_change_exp_nxt12',	(pct_change_Exp_Nxt12_Count), 
				'pct_change_outagehrs',			(pct_change_OutageHrs), 
				'pct_change_reliability',		(pct_change_Reliability),
				'latestpk',						(pk)
				)) WITHIN GROUP (ORDER BY METRIC_TYPE DESC)  AS data,    
		array_agg(
				OBJECT_CONSTRUCT(
				'metric_type',					(Metric_Type),
				'pct_change_count',				(pct_change_count_prv),
				'pct_change_inactive_sfa',		(pct_change_Pending_retirement_prv),
				'pct_change_backlog',			(pct_change_backlog_prv),
				'pct_change_inactive_msa',		(pct_change_Inactive_MSA_prv),
				'pct_change_exp_nxt12',	(pct_change_Exp_Nxt12_Count_prv), 
				'pct_change_outagehrs',			(pct_change_OutageHrs_prv), 
				'pct_change_reliability',		(pct_change_Reliability_prv),
				'latestpk',						(pk)
				)) WITHIN GROUP (ORDER BY METRIC_TYPE DESC)  AS data2 ,    
		array_agg(
				OBJECT_CONSTRUCT(
				'metric_type',					(Metric_Type),
				'pct_change_pending_retirement',(pct_change_Pending_retirement_prv),
				'pct_change_outagehrs',			(pct_change_OutageHrs_prv), 
				'pct_change_reliability',		(pct_change_Reliability_prv),
				'latestpk',						(pk)
				)) WITHIN GROUP (ORDER BY METRIC_TYPE DESC) AS bottom_row_prevmo_change_pcts_vw2			-- SELECT metric_type, pct_change_pending_retirement, pct_change_outagehrs, pct_change_reliability  
																--	FROM 	elevator_bottom_row_prevmo_change_pcts_vw2, 
																--			total_network_bottom_row_prevmo_change_pcts_vw2 order by metric_type
	FROM _LOBBY_BOTTOM_ROW_CHANGE_PCTS;