CREATE OR REPLACE VIEW VW_GB_LOBBY_IMPRESSION_METRICS_CURMO_VS_YRAGO AS
WITH _PREPARED_4JSON AS ( 
		SELECT	
				b.PK,   
				b.Impression_Count,
				b.L7ImpressionsPending, 
				b.Impressions_Inactive_MSA,
				b.Impressions_Exp_Nxt12_Count,
				b.Impressions_MakeGoods,
				b.Impression_Utilization,
				b.actual_utilization,
				h.Total_Impression_BackLog
			FROM 
				VW_LOBBY_IMPRESSION_METRICS_12MOS	 b
					LEFT JOIN	VW_LOBBY_IMPRESSIONBACKLOG_HISTORY_12MONTHS h 
						ON b.PK = h.PK 
			WHERE b.rn IN (1,13)
		)
SELECT 
		array_agg(
			OBJECT_CONSTRUCT(
			'pk', to_char(PK, 'yyyy-mm-dd'),
			'impression_count',		(Impression_Count),
			'l7impressionspending',			(L7ImpressionsPending),
			'total_impression_backlog',		(total_impression_backlog),
			'impressions_inactive_msa',		(impressions_inactive_msa),
			'impressions_exp_nxt12_count',	(impressions_exp_nxt12_count),
			'impressions_makegoods',		(impressions_makegoods), 
			'inventory_utilization',		(Impression_Utilization),
			'actual_utilization',			(actual_utilization)
		)) AS data 
FROM 
	_PREPARED_4JSON;