CREATE OR REPLACE VIEW VW_GB_CAB_METRICS_12MOS_PROMPT COMMENT = 'Needs variables TALEND_COUNTRY,	TALEND_NETWORK' AS
WITH 
_CABBACKLOG_HISTORY_12MONTHS AS	(
						SELECT 
								asofdate AS PK,
								Total_Lobby_BackLog
							FROM  TABLE(GET_CABBACKLOG_HISTORY_12MONTHS($TALEND_COUNTRY,$TALEND_NETWORK))
),
_PREPARED_4JSON AS ( 	
					SELECT	
							b.PK, 
							b.TotalCabs,
							b.Cabs_Inactive_MSA,
							b.Cabs_Exp_Nxt12_Count,
							b.CabOutageHrs_Elev_Bldgs,
							b.TotalCabsPendingRetirement,
							b.CabsOutOfService,
							b.Cab_Reliability,
							h.Total_Lobby_BackLog, 
							b.rn
						FROM 
							VW_CAB_METRICS_12MOS	 b
								LEFT JOIN _CABBACKLOG_HISTORY_12MONTHS h 
										ON b.PK = h.PK	
)
SELECT 
		CASE WHEN
			NOT( $TALEND_COUNTRY = 'ALL' AND $TALEND_NETWORK='ALL' ) 
			THEN 
			array_agg(
					OBJECT_CONSTRUCT(
						'pk', to_char(PK, 'yyyy-mm-dd'),
						'totalcabs',		(TotalCabs),
						'cabs_inactive_msa',			(Cabs_Inactive_MSA),
						'cabs_exp_nxt12_count',		(Cabs_Exp_Nxt12_Count),
						'totalcabspendingretirement',		(TotalCabsPendingRetirement),
						'total_cab_backlog',	(Total_Lobby_BackLog), /*Alias!!!!*/
						'cabsoutofservice',		(CabsOutOfService), 
						'caboutagehrs_elev_bldgs',		(CabOutageHrs_Elev_Bldgs), 
						'cab_reliability',			(Cab_Reliability)
					))
			ELSE
			array_agg(
					OBJECT_CONSTRUCT(
						'pk', to_char(PK, 'yyyy-mm-dd'),
						'total_network_cabs_count',			(TotalCabs),
						'total_network_cabs_inactive_msa',	(Cabs_Inactive_MSA),
						'total_network_cabs_exp_nxt12',		(Cabs_Exp_Nxt12_Count),
						'total_network_cabs_inactivesfa',	(TotalCabsPendingRetirement),
						'total_network_cab_backlog',		(Total_Lobby_BackLog), /*Alias!!!!*/
						'total_network_cabs_outofservice',	(CabsOutOfService), 
						'total_network_cabs_outagehrs',		(CabOutageHrs_Elev_Bldgs), 
						'total_network_cab_reliability',	(Cab_Reliability/2)
					)) 
			END	AS data ,
			$TALEND_COUNTRY AS COUNTRY,
			$TALEND_NETWORK AS NETWORK
	FROM _PREPARED_4JSON;
