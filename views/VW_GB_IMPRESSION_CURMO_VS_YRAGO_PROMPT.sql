CREATE OR REPLACE VIEW VW_GB_IMPRESSION_CURMO_VS_YRAGO_PROMPT  COMMENT = 'Needs variables TALEND_COUNTRY,	TALEND_NETWORK' AS
WITH _IMPRESSBACKLOG_HISTORY_12MONTHS AS
			(
				SELECT 
						asofdate AS PK,
						Total_Impression_BackLog
				FROM  TABLE(GET_IMPRESSBACKLOG_HISTORY_12MONTHS($TALEND_COUNTRY,$TALEND_NETWORK))
),
_PREPARED_4JSON AS ( 	
		SELECT b.PK,   
				b.Impression_Count,
				b.L7ImpressionsPending, 
				b.Impressions_Inactive_MSA,
				b.Impressions_Exp_Nxt12_Count,
				b.Impressions_MakeGoods,
				b.Impression_Utilization,
				b.actual_utilization,
				nvl(h.Total_Impression_BackLog,0) Total_Impression_BackLog
		FROM VW_IMPRESSION_METRICS_12MOS b 
			LEFT JOIN  _IMPRESSBACKLOG_HISTORY_12MONTHS h
				ON b.PK = h.PK 
			WHERE rn IN (1,13)
		)
SELECT 
	CASE WHEN
				NOT( $TALEND_COUNTRY = 'ALL' AND $TALEND_NETWORK='ALL' ) 
		THEN 
		array_agg(
			OBJECT_CONSTRUCT(
			'pk', to_char(PK, 'yyyy-mm-dd'),
			'impression_count',		(Impression_Count),
			'l7impressionspending',			(L7ImpressionsPending),
			'total_impression_backlog',		(total_impression_backlog),
			'impressions_inactive_msa',		(impressions_inactive_msa),
			'impressions_exp_nxt12_count',	(impressions_exp_nxt12_count),
			'impressions_makegoods',		(impressions_makegoods), 
			'inventory_utilization',		(Impression_Utilization), -- Alias! <>
			'actual_utilization',			(actual_utilization)
			)) 
		ELSE
		array_agg(
			OBJECT_CONSTRUCT(
			'pk', to_char(PK, 'yyyy-mm-dd'),
			'total_network_imp_count',			(Impression_Count),
			'total_network_imp_inactivesfa',	(L7ImpressionsPending),
			'total_network_imp_backlog',		(total_impression_backlog),
			'total_network_imp_inactive_msa',	(impressions_inactive_msa),
			'total_network_imp_exp_nxt12',		(impressions_exp_nxt12_count),
			'total_network_imp_makegoods',		(impressions_makegoods), 
			'total_network_imp_utilization',	(Impression_Utilization), -- Alias! <>
			'total_network_act_utilization',	(actual_utilization/2)
			)) 
		END AS data ,
		$TALEND_COUNTRY AS COUNTRY,
		$TALEND_NETWORK AS NETWORK 
FROM _PREPARED_4JSON; 