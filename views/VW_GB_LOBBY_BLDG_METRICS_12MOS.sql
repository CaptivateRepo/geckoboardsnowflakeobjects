CREATE OR REPLACE VIEW VW_GB_LOBBY_BLDG_METRICS_12MOS AS
WITH _PREPARED_4JSON AS ( 
			SELECT	
				B.PK,   
				B.TOTAL_BLDG_BACKLOG,
				S.BLDG_COUNT, 
				S.L7BUILDINGSPENDING, 
				S.BLDGS_INACTIVE_MSA, 
				S.BLDG_EXP_NXT12_COUNT, 
				S.SERVEROUTAGEHRS, 
				S.BLDG_RELIABILITY
			FROM 
				VW_LOBBY_BLDGBACKLOG_HISTORY_12MONTHS	 B
					LEFT JOIN	VW_LOBBY_BLDG_METRICS_12MOS S 
						ON B.PK = S.PK 
)
SELECT 
		ARRAY_AGG(
			OBJECT_CONSTRUCT(
				'pk', to_char(PK, 'yyyy-mm-dd'),
				'total_bldg_backlog',		(TOTAL_BLDG_BACKLOG),
				'bldg_count',				(BLDG_COUNT),
				'l7buildingspending',		(L7BUILDINGSPENDING),
				'bldgs_inactive_msa',		(BLDGS_INACTIVE_MSA),
				'bldg_exp_nxt12_count',		(BLDG_EXP_NXT12_COUNT),
				'serveroutagehrs',			(SERVEROUTAGEHRS),
				'bldg_reliability',			(BLDG_RELIABILITY)
		)) AS data 
	FROM 
		_PREPARED_4JSON;