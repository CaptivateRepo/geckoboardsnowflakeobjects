CREATE OR REPLACE VIEW VW_GB_AD_UTILIZATION_OVERBOOKED AS
WITH _workingdates AS ( SELECT row_number() OVER (ORDER BY cal.MYDATE desc) rn,
							to_date(date_trunc('DAY', cal.MYDATE)) ASOFDATE
						FROM FIVETRAN.PIVOTAL_SFA_DBO.ALLDATES AS cal 
						WHERE cal.ISWKDAY = TRUE 
						AND cal._FIVETRAN_DELETED = FALSE
							AND cal.MYDATE < CURRENT_DATE-1
						ORDER BY cal.MYDATE DESC
),
_prevworkingday AS (
					SELECT w.ASOFDATE FROM _workingdates w WHERE w.rn <=1
),
_calendar  AS (	SELECT ASOFDATE  FROM _prevworkingday
),
_sales_venue_history AS (SELECT sales_venue_id AS sales_venue_key  
							  ,sv.MKT
							  ,sv.venue_name
						      ,c.NAME network
						      ,sv.displays
						      ,sv.avg_min_exposure
						      ,sv.EFFDATE AS eff_date
						       ,sv.CP_INACTIVE
						      ,DATEADD('second', -1, NVL(LEAD(sv.effdate) ignore nulls over (PARTITION BY sv.sales_venue_id order by effdate), to_timestamp_ntz('2099-01-01 00:00:00'))) exp_date
						      ,IFF(ROW_NUMBER()OVER(PARTITION BY sv.sales_venue_id ORDER BY sv.effdate DESC) = 1, 1, 0) AS is_active_value
						    FROM 	fivetran.pivotal_sfa_dbo.sales_venue AS sv   
						    		INNER JOIN  FIVETRAN.PIVOTAL_SFA_DBO.CP_VENUE_NETWORK c 
						    				ON c.CP_VENUE_NETWORK_ID= VENUE_NETWORK
						   WHERE sv._FIVETRAN_DELETED = FALSE
),
_venueplanshort AS 
(		SELECT 			x.MKT,x."NETWORK" ,x.VENUE_NAME,x.CLIENT ,x.TORno,x.SERVER_AME ,x.COR_CPM ,x.plandate,x.FLT_ROTATION,x.FLT_START_HR ,x.FLT_END_HR ,x.NO_VENUE_SERVERS
												,x.SIC1 ,x.duration,x.VENUE_AME
						FROM ( 
											 SELECT			
											 		 v.MKT,v."NETWORK" ,v.VENUE_NAME,v.CLIENT ,v.TORno,v.SERVER_AME ,v.COR_CPM ,v.plandate,v.FLT_ROTATION,v.FLT_START_HR ,v.FLT_END_HR ,v.NO_VENUE_SERVERS
													,v.SIC1 ,v.duration,v.VENUE_AME,
													IFF(rank() OVER (PARTITION BY v.torno,v.MKT,v."NETWORK" ,v.VENUE_NAME,v.plandate ORDER BY createdate desc)  = 1, 1, 0) AS is_active_value
												FROM ( 
														SELECT 
															v.MKT,v."NETWORK" ,v.VENUE_NAME,v.CLIENT ,v.TORno,v.SERVER_AME ,v.COR_CPM ,v.plandate,v.FLT_ROTATION,v.FLT_START_HR ,
															v.FLT_END_HR ,v.NO_VENUE_SERVERS
															,v.SIC1 ,v.duration , v.createdate,v.VENUE_AME
														FROM FIVETRAN.SQL_SERVER_DBO.VENUEPLAN AS v 
																INNER JOIN _calendar D ON D.ASOFDATE = v.plandate
														WHERE  v._fivetran_deleted = FALSE 
																AND server_AME>0				
													) v
											)	x	WHERE X.is_active_value	 = 1 
						GROUP BY  x.MKT,x."NETWORK", x.VENUE_NAME,x.CLIENT, x.TORno, x.SERVER_AME, x.COR_CPM, x.plandate, x.FLT_ROTATION, x.FLT_START_HR, x.FLT_END_HR, x.NO_VENUE_SERVERS, 
												x.SIC1, x.duration, x.VENUE_AME		
						),
_venueplan AS (
						SELECT 	v.* ,
								SERVER_count  AS numsrvrs, 
								p.BARTER , D.ASOFDATE
						FROM 
							_venueplanshort v 
							INNER JOIN _sales_venue_history dsv ON trim(dsv.venue_name) = trim(v.venue_name) AND v.PLANDATE 
															BETWEEN dsv.eff_date AND dsv.exp_date  
															AND dsv.MKT = v.MKT 
															AND v."NETWORK" = dsv.NETWORK
							INNER JOIN _calendar D ON D.ASOFDATE = v.plandate
							INNER JOIN FIVETRAN.PIVOTAL_SFA_DBO.SERVER_COUNT_VW  cnt  ON cnt.SALES_VENUE_ID  = dsv.sales_venue_key 
							INNER JOIN FIVETRAN.PIVOTAL_SFA_DBO.CP_PROPOSAL p ON P.PROPOSAL_NUMBER = v.TORno
							LEFT OUTER JOIN FIVETRAN.PIVOTAL_SFA_DBO.SIC b ON b.SIC_CODE = v.sic1
							WHERE 1=1
									AND dsv.is_active_value=1
									AND p._FIVETRAN_DELETED = FALSE
									AND b._FIVETRAN_DELETED = FALSE
									AND p.IS_TRAFFIC_ORDER = 1
)
SELECT array_agg(
		OBJECT_CONSTRUCT(
		'asofdate', to_char(ASOFDATE, 'yyyy-mm-dd'),
		'mkt',mkt,
		'network',					NETWORK,
		'venue_name',				VENUE_NAME,
		'utilization_revenue', 		(UTILIZATION_REVENUE),
		'utilization_nonrevenue',	(UTILIZATION_NONREVENUE),
		'utilization_total',		(UTILIZATION_TOTAL)
		)) WITHIN GROUP (ORDER BY ASOFDATE ASC) AS DATA 
	FROM ( 
				SELECT 		A.NETWORK,A.VENUE_NAME,A.MKT,A.ASOFDATE,
							SUM(A.TOTALSPOTS)/2640*100 AS UTILIZATION_TOTAL,
							SUM(A.TOTALSPOTS_OLD)/2640*100 AS UTILIZATION_TOTAL_OLD,
							SUM( IFF( A.BARTER   = 0  AND A.COR_CPM   > 0,	1,0	)	*	A.TOTALSPOTS )	/ 2640 * 100 	AS UTILIZATION_REVENUE,
							SUM( IFF( A.BARTER   = 1  OR A.COR_CPM   = 0,	1,0	)	*	A.TOTALSPOTS )	/ 2640 * 100	AS UTILIZATION_NONREVENUE,
							ROW_NUMBER() OVER (ORDER BY UTILIZATION_TOTAL DESC) RN
						FROM (
								SELECT ((V.FLT_END_HR - V.FLT_START_HR) * V.FLT_ROTATION )/NUMSRVRS AS TOTALSPOTS_OLD, 
									 	((FLT_END_HR - FLT_START_HR) * FLT_ROTATION) * DURATION /15 /NUMSRVRS TOTALSPOTS
										,V.* 
									FROM  _VENUEPLAN V
										WHERE  V.VENUE_AME>0
							) A
							GROUP BY NETWORK,VENUE_NAME,MKT, ASOFDATE
		) WHERE RN <=10
ORDER BY UTILIZATION_TOTAL DESC;
